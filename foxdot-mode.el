(defvar foxdot-buffer-name "*FoxDot*")

;; (defvar foxdot-cli-path "/home/laurent/.local/apps/foxdot/")

(defun foxdot-start ()
  (interactive)
  (progn
    (setq
     python-shell-interpreter-args (concat "-i " foxdot-cli-path "foxdot-cli.py")
     python-shell-completion-native-enable nil
     fd-buffer (format (buffer-name))
     )
    (run-python (python-shell-parse-command))
    ;; (run-python)
    (python-shell-switch-to-shell)
    ;; (rename-buffer foxdot-buffer-name)
    (fit-window-to-buffer nil nil 8)
    (switch-to-buffer-other-window fd-buffer)
    ))

(defun foxdot-start-sclang ()
  (interactive)
  (progn
    (setq
     fd-buffer (format (buffer-name))
     scd-buffer (find-file (concat foxdot-cli-path "/foxdot.scd"))
     )
    (sclang-start)
    (sit-for 3)
    (set-buffer scd-buffer)
    (sclang-eval-document)
    (sit-for 4)
    (switch-to-buffer fd-buffer)
    (foxdot-start)
    )
  )

(defun foxdot-execute(start end)
  (interactive "r")
  (progn
    (setq
     fd-code (buffer-substring-no-properties start end)
     fd-code-buffer (format (buffer-name))
     )
    ;; (append-to-buffer (get-buffer foxdot-buffer-name) start end)
    ;; (append-to-buffer "go")
    (switch-to-buffer-other-window (get-buffer foxdot-buffer-name))
    (insert fd-code)
    (insert "\ngo")
    (execute-kbd-macro "\C-m")
    (switch-to-buffer-other-window fd-code-buffer)
    ;; (execute-kbd-macro "\C-g")
    ))

(defun foxdot-region()
  (interactive)
  (message "fd-region")
  (progn
    (save-excursion
      (mark-paragraph)
      (setq
       fd-code (buffer-substring-no-properties (+ (region-beginning) 1) (region-end))
       )

      (python-shell-send-string fd-code)
      ;; (foxdot-send-string fd-code)
      ;; (python-shell-send-region (region-beginning) (region-end))

      ;; (foxdot-execute (region-beginning) (region-end))
      (deactivate-mark t)
      ;; (pulse-momentary-highlight-region (mark) (point))
      (pulse-momentary-highlight-region (+ (region-beginning) 1) (region-end))
      ;; (sit for 0.25)
      (run-at-time "0.2 sec" nil #'pulse-momentary-unhighlight)
      )
    )
  )


(defun foxdot-stopall()
  (interactive)
  (message "fd-stop")
  (python-shell-send-string "Clock.clear()")
  ;; (progn
  ;;   (switch-to-buffer-other-window (get-buffer foxdot-buffer-name))
  ;;   (insert "Clock.clear()\ngo")
  ;;   (execute-kbd-macro "\C-m")
  ;;   (switch-to-buffer-other-window fd-code-buffer)
  ;;   )
  )


(setq foxdot-keywords
'("blip"
  "bass"
  "sawbass"
  "pluck"
  "formant"
  "dirt"
  ))

(defun foxdot-test-func ()
  "print a message"
  (interactive)
  (setq mode-name "FoxDot")
  (message mode-name))

(defun foxdot-test-func-ret ()
  "print a message"
  (interactive)
  (message "this is a test with ret"))

(defun foxdot-completion-at-point ()
  "This is the function to be used for the hook `completion-at-point-functions'."
  (interactive)
  (let* (
         (bds (bounds-of-thing-at-point 'symbol))
         (start (car bds))
         (end (cdr bds)))
    (list start end foxdot-keywords . nil )))


(defun foxdot-next ()
  (interactive)
  (evil-forward-sentence-begin)
  (evil-forward-sentence-begin)
  )

(defun foxdot-previous ()
  (interactive)
  (evil-backward-sentence-begin)
  (evil-backward-sentence-begin)
  )

;; (global-set-key [?\C-c ?\C-e] `foxdot-execute)
;; (global-set-key [?\C-c ?\C-f] `foxdot-start)
;; (global-set-key [?\M-.] `foxdot-stopall)
;; (global-set-key [?\M-\r] `foxdot-region) 
;; (global-set-key [?\C-c ?\C-c] `foxdot-region) 

(defvar foxdot-mode-map
  (let ((map (make-sparse-keymap)))
    ;; (global-set-key [?\C-c ?\C-e] `foxdot-execute)
    (global-set-key [?\C-c ?\C-f] `foxdot-start)
    (define-key map (kbd "C-c m") 'foxdot-test-func)
    (define-key map (kbd "C-.") 'foxdot-stopall)
    (define-key map (kbd "<C-return>") `foxdot-region)
    (define-key map (kbd "<M-return>") `foxdot-region)
    (define-key map (kbd "C-c h") 'foxdot-stopall)
    (define-key map (kbd "C-c f") 'foxdot-next)
    (define-key map (kbd "C-c b") 'foxdot-previous)
    (define-key map [?\C-c ?\C-c] `foxdot-region)
    (define-key map [?\C-c ?\C-s] 'foxdot-stopall)
    map))

;; (defun foxdot-mode()
;;   "Major mode for FoxDot"
;;   (kill-all-local-variables)
;;   (use-local-map foxdot-mode-map)
;;   (setq major-mode 'foxdot-mode)
;;   (setq mode-name "FoxDot")
;;   (add-hook 'completion-at-point-functions 'foxdot-completion-at-point nil 'local)
;;   )

;;;###autoload
(define-derived-mode foxdot-mode python-mode "FoxDot"
  "Major Mode for FoxDot"
  ;; (kill-all-local-variables)
  (setq mode-name "FoxDot")
  ;; (use-local-map foxdot-mode-map)
  (add-hook 'web-mode-hook (lambda () (eldoc-mode -1)))
  (add-hook 'completion-at-point-functions 'foxdot-completion-at-point nil 'local)
  )

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.fox?\\'" . foxdot-mode))

(provide 'foxdot-mode)
