import colour

print("startup script for foxdot")

Server.add_forward("localhost", 12345)

s = ServerManager("localhost", 6669)

Scale.default = 'minor'

def layout(val):
    if val == "lines":
        m = OSCMessage("/parameters/layout_seq", 0)
        s.sendOSC(m)
        m = OSCMessage("/parameters/layout_lines", 1)
        s.sendOSC(m)
    elif val == "seq":
        m = OSCMessage("/parameters/layout_lines", 0)
        s.sendOSC(m)
        m = OSCMessage("/parameters/layout_seq", 1)
        s.sendOSC(m)

def color_mode(val):
    if val == "notes":
        m = OSCMessage("/parameters/color_mode", 0)
        s.sendOSC(m)
    elif val == "instrument":
        m = OSCMessage("/parameters/color_mode", 1)
        s.sendOSC(m)

def loop_length(val):
    m = OSCMessage("/parameters/seq_length", val)
    s.sendOSC(m)

def fade(start, end, t): return linvar([start, end], [t, inf], start= now)
def fade_in(t): return linvar([0, 1], [t, inf], start= now)
def fade_out(t): return linvar([1, 0], [t, inf], start= now)

def col(val):
    c = colour.Color()
    if type(val) == str:
        c = colour.Color(val)
    elif type(val) == tuple:
        c = colour.Color(rgb=val)
    val_out = int("0x"+c.get_hex_l()[1:], 16)
    return val_out
