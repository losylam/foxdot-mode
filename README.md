# FoxDot major mode for emacs

Derived from [this gist](https://gist.github.com/lvm/c84e4d5ca54f1db256eaa6c98db5b141) by [Mauro](https://www.cyberpunk.net.ar/) 

Tested with emacs/spacemacs 27
