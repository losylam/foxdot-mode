from foxdot import *

exec(open("startup.py").read())

def layout(val):
    if val == "lines":
        m = OSCMessage("/parameters/layout_seq", 0)
        s.sendOSC(m)
        m = OSCMessage("/parameters/layout_lines", 1)
        s.sendOSC(m)
    elif val == "seq":
        m = OSCMessage("/parameters/layout_lines", 0)
        s.sendOSC(m)
        m = OSCMessage("/parameters/layout_seq", 1)
        s.sendOSC(m)

class PBounce(GeneratorPattern):
    def __init__(self, l, step=4, offset=1):
        GeneratorPattern.__init__(self)
        self.l = l 
        self.step = step
        self.offset = offset
    def func(self, index):
        if (index+self.offset)% self.l == 0:
            return self.step
        else:
            return 0

# p1 >> blip(PBounce(3, 4, 0)[:16] | PBounce(5, 2, 0)[:8], dur=1/4, pan=[-1, 1, 0])
# p2 >> play('Xs', sample=2)
# d1 >> dbass(dur=PDur(3, 8), dirt=1)

def m(o, a):
    return o + a*lfo
# p1 >> saw(dur=1/4, amp=1, lpf=m(400, 2000))

Scale.default="minor"
Root.default = -2
