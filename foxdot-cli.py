#!/usr/bin/env python
import sys
import os
import cmd
import subprocess
import time

START_SC = False

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# ACTIVATE_PY = os.path.join(BASE_PATH, "venv-pitchglitch", "bin", "activate_this.py")
# exec(compile(open(ACTIVATE_PY, "rb").read(), ACTIVATE_PY, 'exec'), dict(__file__=ACTIVATE_PY))

if START_SC:
    subprocess.Popen(['sclang', os.path.join(BASE_PATH, 'foxdot.scd')])
    time.sleep(2)

sys.path.append(os.path.join(BASE_PATH))
from FoxDot_lo import *

def fade(start, end, t): return linvar([start, end], [t, inf], start= now)
def fadein(t): return linvar([0, 1], [t, inf], start= now)
def fadeout(t): return linvar([1, 0], [t, inf], start= now)

## FoxDot to ofxpartloop
Server.add_forward("localhost", 12345)
s = ServerManager("localhost", 6669)

def layout(val):
    if val == "lines":
        m = OSCMessage("/parameters/layout_seq", 0)
        s.sendOSC(m)
        m = OSCMessage("/parameters/layout_lines", 1)
        s.sendOSC(m)
    elif val == "seq":
        m = OSCMessage("/parameters/layout_lines", 0)
        s.sendOSC(m)
        m = OSCMessage("/parameters/layout_seq", 1)
        s.sendOSC(m)

def color_mode(val):
    if val == "notes":
        m = OSCMessage("/parameters/color_mode", 0)
        s.sendOSC(m)
    elif val == "instrument":
        m = OSCMessage("/parameters/color_mode", 1)
        s.sendOSC(m)

def loop_length(val):
    m = OSCMessage("/parameters/seq_length", val)
    s.sendOSC(m)

## FoxDot to tui-seq
client = OSCClientWrapper()
client.connect(('localhost', 17002))

def send_beat():
    b = int(Clock.now()%4)
    m = OSCMessage('/fd/clock')
    m.append(b)
    client.send(m)
    Clock.schedule(lambda: send_beat(), Clock.next_bar()+Clock.seconds_to_beats(0.25))
# send_beat() in foxdot to activate

def transpose(value):
    m = OSCMessage('/fd/transpose', value)
    client.send(m)
# (use with sync_to to transpose)
# c1>> pulse([0, 4], dur=3)
# c1.sync_to(transpose)


if __name__ == "__main__":
    print('-=-=-=-= FoxDot (%s) =-=-=-=-' % FoxDot.__version__)

